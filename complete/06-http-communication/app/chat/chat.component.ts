import { Component, OnInit, OnDestroy } from '@angular/core';
import { ChatRoom, Message } from "../model/chat";
import { Http, Response } from "@angular/http";

import 'rxjs/add/operator/map'
import 'rxjs/add/operator/toPromise'
import { Observable, Subject, Subscription } from "rxjs";
import { HttpClient } from '@angular/common/http';

@Component({
  selector: 'app-chat',
  templateUrl: './chat.component.html',
  styleUrls: ['./chat.component.css']
})
export class ChatComponent implements OnInit, OnDestroy {

  readonly CHATROOM_URL = 'http://localhost:8000/rooms/1';

  chatRoom: ChatRoom;

  myMessages$ = new Subject();
  myMessagesStream$: Observable<Response>;
  chatRoomStream$: Subscription;

  constructor(private http: HttpClient) {

    this.myMessagesStream$ = this.myMessages$
      .flatMap(message => this.http.post(this.CHATROOM_URL, message));
  }

  ngOnInit(): void {

    this.chatRoomStream$ = Observable.merge(Observable.interval(4000), this.myMessagesStream$)
      .startWith(null)
      .flatMap(() => this.http.get<ChatRoom>(this.CHATROOM_URL))
      .do(() => console.log('chatReloaded'))
      .subscribe((chatRoom) => this.chatRoom = chatRoom);
  }

  sendMyMessage(message: Message) {
    this.myMessages$.next(message);
  }

  ngOnDestroy() {
    this.chatRoomStream$.unsubscribe();
  }
}
