import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { FormsModule } from '@angular/forms';
import { HttpModule } from '@angular/http';

import { AppComponent } from './app.component';
import { OrdersViewerComponent } from './orders-viewer/orders-viewer.component';
import { OrderComponent } from './order/order.component';
import { OrderAddressComponent } from './order-address/order-address.component';
import { OrdersListComponent } from './orders-list/orders-list.component';

@NgModule({
  declarations: [
    AppComponent,
    OrdersViewerComponent,
    OrderComponent,
    OrderAddressComponent,
    OrdersListComponent
  ],
  imports: [
    BrowserModule,
    FormsModule,
    HttpModule
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
