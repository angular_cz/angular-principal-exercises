import { Component, OnInit } from '@angular/core';
import { ChatRoom, Message } from '../model/chat';

import { Observable, Subject } from 'rxjs';

import 'rxjs/add/operator/startWith';
import 'rxjs/add/operator/mergeMap';
import 'rxjs/add/operator/do';
import 'rxjs/add/operator/skipWhile';

import { ActivatedRoute, Params } from '@angular/router';
import { ChatService } from '../chat.service';

@Component({
  selector: 'app-chat-detail',
  templateUrl: './chat-detail.component.html'
})
export class ChatDetailComponent implements OnInit {

  chatRoomId = 1;
  chatRoom: ChatRoom;
  myMessages$ = new Subject<Message>();
  myMessagesStream$: Observable<Response>;
  routeParams$: Observable<Params>;

  constructor(private chatService: ChatService,
              private route: ActivatedRoute) {

    this.routeParams$ = this.route.params
      .do(params => this.chatRoomId = parseInt(params['id']));

    this.myMessagesStream$ = this.myMessages$
      .flatMap(message => this.chatService.sendMessage(this.chatRoomId, message));
  }

  ngOnInit(): void {

    Observable.merge(Observable.interval(5000),
      this.myMessagesStream$,
      this.routeParams$
    )
      .startWith(null)
      .flatMap(() => this.chatService.getRoomById(this.chatRoomId))
      .do(() => console.log('chatReloaded'))
      .subscribe((chatRoom) => this.chatRoom = chatRoom);
  }

  sendMyMessage(message: Message) {
    this.myMessages$.next(message);
  }
}
